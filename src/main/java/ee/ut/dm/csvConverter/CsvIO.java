package ee.ut.dm.csvConverter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class CsvIO {
	private final static String CSV_SPLIT_BY = ";";
	
	public static List<String> findAllFeatures(String path, int minCount) {
		System.out.println("Starting to search for all features in the CSV!");
		int lineNr = 0;
		BufferedReader br = null;
		String line = "";
		HashMap<String, Integer> allFeatures = new HashMap<String, Integer>();
		
		try {
			br = new BufferedReader(new FileReader(path));
			
			while ((line = br.readLine()) != null) {
				if(lineNr == 0) { //skip header
					lineNr++;
					continue;
				}
				
				String[] splitRow = line.split(CSV_SPLIT_BY);
				
				for(int i = 2; i < splitRow.length; i++) { // i=2 -> skip sex and age
					if(!allFeatures.containsKey(splitRow[i])) {
						allFeatures.put(splitRow[i], 1);
					} else {
						allFeatures.put(splitRow[i], allFeatures.get(splitRow[i]) + 1);
					}
				}
					
				if(lineNr % 100000 == 0) { //debug
					System.out.println("Searching for unique features - current line: " + lineNr);
				}
				lineNr++;
			}
			
		} catch(FileNotFoundException e) {
			System.out.println("File not found: " + path);	
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		Iterator<Entry<String, Integer>> it = allFeatures.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>)it.next();
	        if(pair.getValue() <= minCount) { //skip features, which appear less than X times
	        	it.remove();
	        }
	    }
	    
		List<String> featureList = new ArrayList<String>();
		featureList.addAll(allFeatures.keySet());
		Collections.sort(featureList);	
		System.out.println("Feature search complete!");
		System.out.println("Found features: " + (featureList.size() + 3)); // +3 for id, sex and age
		
		return featureList;
	}
	
	public static void writeFeatureHeader(List<String> featureList) {
		FileWriter writer = null;
		try {
			writer = new FileWriter("output/converted_header");
			writer.append("id;gender;age;" + StringUtils.join(featureList.toArray(), CSV_SPLIT_BY) + "\r\n"); //write header
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static List<String> importFeatureHeader() {
		BufferedReader br = null;
		String line = "";
		List<String> featureHeader = new ArrayList<String>();
		
		try {
			br = new BufferedReader(new FileReader("output/converted_header"));
			
			while ((line = br.readLine()) != null) {				
				String[] splitRow = line.split(CSV_SPLIT_BY);
				featureHeader.addAll(Arrays.asList(splitRow));
			}
			
		} catch(FileNotFoundException e) {
			System.out.println("File not found: output/converted_header");	
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return featureHeader;
	}
	
	public static void convertCsv(String path, List<String> featureList, Map<String, Integer> featureSet) {
		System.out.println("Starting to convert CSV : " + path + "!");
		
		BufferedReader br = null;
		FileWriter writerMales = null;
		FileWriter writerFemales = null;

		
		try {
			writerMales = new FileWriter("output/converted_Male_"+ path);
			writerFemales = new FileWriter("output/converted_Female_"+ path);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		String line = "";	
		int lineNr = 0;
		
		try {
			br = new BufferedReader(new FileReader("input/" + path));
			
			while ((line = br.readLine()) != null) {				
				if(lineNr % 1000 == 0) {
					System.out.println("Converting CSV (" + path + ") - current line: " + lineNr);
				}
				StringBuilder sb = new StringBuilder();
				
				String[] splitRow = line.split(CSV_SPLIT_BY);
				
				if(splitRow.length <= 3) {
					lineNr++;
					continue; //only id, age and sex, skip this row
				}
				
				String[] featureOutput = new String[featureList.size()];

				featureOutput[0] = splitRow[0]; //id
				featureOutput[1] = splitRow[1]; //sex
				featureOutput[2] = splitRow[2]; //age

				for(int i = 3; i < splitRow.length; i++) {
					if(featureSet.containsKey(splitRow[i])) {		
						featureOutput[featureSet.get(splitRow[i])] = "1";
					}
				}
					
				sb.append(featureOutput[0]); 
				sb.append(";");
				sb.append(featureOutput[1]);
				sb.append(";");
				sb.append(featureOutput[2]);
				for(int i = 3; i < featureOutput.length; i++) {
					sb.append(";");
					if(featureOutput[i] == "1") {
						sb.append("1");
					} else {
						sb.append("0");
					}
				}

				if("F".equals(featureOutput[1])) {
					writerFemales.append(sb.toString());
					writerFemales.append("\r\n");
				} else {
					writerMales.append(sb.toString());
					writerMales.append("\r\n");
				}
				
				lineNr++;
			}			
		} catch(FileNotFoundException e) {
			System.out.println("File " + path + " not found: " + path);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
				writerFemales.close();
				writerMales.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		System.out.println("CSV (" + path + ") conversion complete!");
	}

	public static void createIds(String fileName) {
		System.out.println("Starting to generate ID-s in input file.");
		PrintWriter writer = null;

		try {
			writer = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".temp")));
		} catch (IOException e2) {
			System.out.println("Could not write to file " + fileName + ".temp");
		}
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e1) {
			System.out.println("Could not find file " + fileName);
		}
		
		String line = null;
		int lineNr = 0;
		
		try {
			while ((line = br.readLine()) != null) {
				if(lineNr == 0) { //header
					if(line.startsWith("id;")) {
						System.out.println("Looks like the file " + fileName + " already contains ID-s, skipping generation.");
						return;
					}
					lineNr++;
				    writer.println("id;" + line);
					continue;
				} else {
					if(lineNr % 100000 == 0) {
						System.out.println("Generating ID-s, current line " + lineNr);
					}
				    writer.println(lineNr + ";" + line);
					lineNr++;
				}
			}
		} catch (IOException e) {
			System.out.println("Unable to add ID-s to file: " + fileName);
		} finally {
			try {
				br.close();
				writer.close();
			} catch (IOException e) {}
		}
		
		System.out.println("Renaming temporary file ...");
		File originalFile = new File(fileName);
		originalFile.delete(); // remove the old file
		new File(fileName + ".temp").renameTo(originalFile); // Rename temp file
		System.out.println("ID generation finished!");
		System.out.println("Run the program again after splitting the input file.");
		System.exit(0);
	}
	
	public static void randomSample(int numParts, int sampleCount) {
		System.out.println("Starting to generate random sample.");
		int lineNr = 0;
        String currentLine = null;
        List <String> reservoirListMen = new ArrayList<String>(sampleCount); 
        List <String> reservoirListFemale = new ArrayList<String>(sampleCount); 

        int maleCount = 0;
        int femaleCount = 0;
        
        Random r = new Random();
        int randomNumber = 0;
        
        for(int i = 0; i < numParts; i++) {
	        try {
				Scanner scMale = new Scanner(new File("output/converted_Male_crm_anon." + String.format("%03d", i))).useDelimiter("\r\n");
				Scanner scFemale = new Scanner(new File("output/converted_Female_crm_anon." + String.format("%03d", i))).useDelimiter("\r\n");
				
            	System.out.println("Getting random sample of data. Processing file " + "output/converted_Male_crm_anon." + String.format("%03d", i));

				while(scMale.hasNext()) {
		            currentLine = scMale.next();
		            maleCount++;
	
			        if (maleCount <= sampleCount) {
			        	reservoirListMen.add(currentLine);
			        } else if ((randomNumber = (int) r.nextInt(maleCount)) < sampleCount) {
			            reservoirListMen.set(randomNumber, currentLine);
			        }
				}
				
            	System.out.println("Getting random sample of data. Processing file " + "output/converted_Female_crm_anon." + String.format("%03d", i));

				while(scFemale.hasNext()) {
		            currentLine = scFemale.next();
		            femaleCount++;
	
			        if (femaleCount <= sampleCount) {
			        	reservoirListFemale.add(currentLine);
			        } else if ((randomNumber = (int) r.nextInt(femaleCount)) < sampleCount) {
			        	reservoirListFemale.set(randomNumber, currentLine);
			        }
				}
				
				scMale.close();
				scFemale.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
        }
        System.out.println("Writing output of random sample!");
        try {
			PrintWriter writerMen = new PrintWriter(new BufferedWriter(new FileWriter("output/men_randomSubset.csv")));
			PrintWriter writerFemale = new PrintWriter(new BufferedWriter(new FileWriter("output/female_randomSubset.csv")));

			for(String line : reservoirListMen) {
				writerMen.println(line);
			}
			for(String line : reservoirListFemale) {
				writerFemale.println(line);
			}
			writerMen.close();
			writerFemale.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        System.out.println("Finished getting random sample!");
	}
}
