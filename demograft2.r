#Fixing skewed data, Lauri Tammeveski, Kaupo Kuresson, Univeristy of Tartu

rm(list=ls())
gc()
memory.size()

data = read.csv("converted_Female_crm_anon.013.csv", sep=";", header=FALSE)
test = read.csv("converted_Male_crm_anon.018.csv", sep=";", header=FALSE)

names = read.table("converted_header.txt", sep=";", header=TRUE)
names = colnames(names)

#Sample over all the data
data = read.csv("female_randomSubset.csv", sep=";", header=FALSE)
test = read.csv("men_randomSubset.csv", sep=";", header=FALSE)

test = test[1:30000,]
data = data[1:30000,]

test$V1 <- NULL
test$V2 <- NULL
test$V3 <- NULL
data$V1 <- NULL
data$V2 <- NULL
data$V3 <- NULL

names = names[-2]
indexes = data$V1
indexes.test = test$V1



#--------HEATMAP----------
dev.off()
library(gplots)
rows = 10000
cols = 800
#ord = order(colSums(data[, 1:cols]), decreasing = TRUE)
gc()
heatmap.2(as.matrix(data[60001:90000, ]), dendrogram = "none", hclustfun = "none",
          , Rowv = FALSE, Colv = FALSE, density.info="none", trace="none", labRow = "", labCol = ""
          , col = c("#204CDC", "#E0E3EE"), key = FALSE, xlab="Attributes", ylab="Rows", main = "Correct labels") 

heatmap.2(as.matrix(test[60001:90000, ]), dendrogram = "none", hclustfun = "none",
          , Rowv = FALSE, Colv = FALSE, density.info="none", trace="none", labRow = "", labCol = ""
          , col = c("#204CDC", "#E0E3EE"), key = FALSE, xlab="Attributes", ylab="Rows", main = "Unlabeled data") 
gc()

#------LINEAR MODEL---------

model1 <- lm(V2 ~ ., data = data[, 1:200])
pred1 = predict(model1, newdata = test[, 1:200])

errors <- data.frame(error1 = residuals(model1), error2 =
                       residuals(model2))
dataEllipse(errors$error1, errors$error2, xlab = expression(error[1]),
            ylab = expression(error[2]))

#------------SVM ONE CLASSIFICATION------------

library(e1071)
cols = 1035
rows = 10000
for (nu in c(0.001, 0.01, 0.1, 0.2, 0.3)) {
#cost doesnt efect in onec-lassificaition. only nu
#for (cost in c(0.001, 0.01, 0.1, 1, 10, 100, 1000, 10000, 1000000, 10000000)) {
  for (gamma in c(0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 0.5, 1)) {
    model <- svm(x = data[1:rows,1:cols], y = NULL, type='one-classification', scale=FALSE, nu = 0.1, gamma=0.01)
    pred = predict(model, data[1:rows, 1:cols])
    pred2 = predict(model, data[(rows + 1):30000, 1:cols])
    pred3 = predict(model, test[, 1:cols])
    cat(sprintf("gamma %1.3f \t nu %1.3f \t Train %1.3f \t Test %1.3f \t Unlabaled %1.3f \n", 0, 0, sum(pred)/rows, sum(pred2)/(30000-rows), sum(pred3)/30000))
    pred = predict(model, comb[, c(3:cols)])
    gc()
  }
}

#model <- svm(gender ~ ., data = data[1:rows,c(1,3:cols)], type='one-classification', scale=FALSE, nu = 0.01, kernel = "linear")


#-----------APRIORI-------------

cols = 1036
fdata = data[1:30000, ]
fdata = test[1:30000,]
colnames(fdata) = names
colnames(data) = names
fdata[,names] <- lapply(fdata[,names] , factor)
fdata <- lapply(fdata , factor)
library(arules)

no = paste0(names[-1], sep = "=0")
rulesF = apriori(fdata, parameter = list(target="rules",supp=0.03, conf=0.03, minlen=2),appearance = list(lhs=c("gender=FALSE"),none=as.vector(no), default="rhs"))
rulesM = apriori(fdata, parameter = list(target="rules",supp=0.03, conf=0.03, minlen=2),appearance = list(lhs=c("gender=M", "gender="),none=as.vector(no), default="rhs"))

rules.tbl <- as(rulesF, "data.frame")
index <- sort(rules.tbl$support, decreasing = TRUE, index.return=TRUE)$ix
rules.tbl[index[1:50],]
test.rules = rules.tbl[index[1:50],]
gc()

#rules = apriori(fdata, parameter = list(target="rules",supp=0.01, conf=0.03, minlen = 2),appearance = list(rhs=c("gender=F"),none=as.vector(no), default="lhs"))

